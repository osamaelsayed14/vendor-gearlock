# GearLock integration with Android-x86 source

Currently patches are only available for nougat, oreo, pie and q.
Although other versions are supported by the `executable-gearlock-installer`.
Patch files for murshmellow and lolipop will be included later on.

Author: @AXIM0S 
Source: https://github.com/AXIM0S/bootable-gearlock
Adaptation for Android-Generic Project: @electrikjesus

### Just do

```bash
git clone https://gitlab.com/AXIM0S/vendor-gearlock vendor/gearlock
```
## Proprietary Setup

From your project folder (ex: user/aosp) do:

```bash
. build/envsetup.sh
apply-gearlock-patches
```
This will ask you what version of android you are applying it on
(android-7, android-8, android-9, android-10), Just answer it, 
and it will apply the patches for that version

## Then build ISO.

Android-Generic (x86/PC):
build-x86 android_x86_64-userdebug

BlissOS 11.x:
bash build-x86.sh android_x86_64-userdebug

Android-x86:
lunch android_x86_64-userdebug
make iso_img

# Full Developer Documentation

https://supreme-gamers.com/gearlock
